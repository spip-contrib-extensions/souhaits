<?php

function formulaires_configurer_souhaits_saisies_dist() {
	$saisies = [
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'public',
				'label' => 'Site public',
			],
			'saisies' => [
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'public/masquer_formulaire',
						'label_case' => 'Ne pas afficher le formulaire pour offrir',
					],
				]
			],
		]
	];
	
	return $saisies;
}
